const accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU2OGNkZWZlM2Q1YTc0MWVkODI5ZDcwMGFjN2RlMGI3NGY3ZjU4ZTQyMDkyMjQ5MDhhZjhkZmM1YmFjYTM4NTc0YWIxODcxNzQ2NTBlZjQ3In0.eyJhdWQiOiIyMTQ2MmQyNi0wZmFjLTQzMTgtOTI4MC02MDNmMDAzYjhmOWUiLCJqdGkiOiI1NjhjZGVmZTNkNWE3NDFlZDgyOWQ3MDBhYzdkZTBiNzRmN2Y1OGU0MjA5MjI0OTA4YWY4ZGZjNWJhY2EzODU3NGFiMTg3MTc0NjUwZWY0NyIsImlhdCI6MTcwNzg0OTEzMiwibmJmIjoxNzA3ODQ5MTMyLCJleHAiOjE3MDc5MzU1MzIsInN1YiI6IjEwNjY5MTM0IiwiZ3JhbnRfdHlwZSI6IiIsImFjY291bnRfaWQiOjMxNTY2NDA2LCJiYXNlX2RvbWFpbiI6ImFtb2NybS5ydSIsInZlcnNpb24iOjIsInNjb3BlcyI6WyJwdXNoX25vdGlmaWNhdGlvbnMiLCJmaWxlcyIsImNybSIsImZpbGVzX2RlbGV0ZSIsIm5vdGlmaWNhdGlvbnMiXSwiaGFzaF91dWlkIjoiZTg1NzdkNzUtNmE3YS00OWZjLTk3MWMtYWQyMWExZTczZTA1In0.iHGLWFP8kUON3AzgjniZR6RBFlnm57mUs1T43-u1qhWeLSlF1KZYstK9jCWfaWtweb8VwjKalk0ysafkmU7YVwDaKrVjrpP8Lvaj_hEIRNkJs--CX1U61AS7elxAuHODVymlOX-AcamAuBjjaBBDZrh5OtatMR4fJaSN3l07uIsknoiNJJRtzwvHeGDz1yrYbOMgstnEv5O5hYR2QXPWQ79nSiYMi2M2raWNRCNcw6poWsp5luiIYMp55tgHvrYRshvHizdcrwkIkwW-hT5z4DQAjrfP2c6GDHxFOXSOVPuzIfP8vOMVNXoC0DR4DEImPsirgEH_34973GeCNOMibA";
const refreshToken = "def50200bce3cf69f7a1747beeecc5c6ef822e86a6daafc11fda34b199e0195210433287e0e188fc8be5d01639efaaf9cc725d935139d20d153f0c5a8e061dcdc3b85193e926383fd29752df30d416083f4083974d921f7365864d694832857152f402bb82374864927ab4fa629314e5dab70c63724031439df7566014df269ea01fee3437eb5b108469cab235f980041b2e2a0fee4f8fa14482736741da7f1b11166eae3626285a5e1c1bca7b31c8f7c74e50ba36f70a97dda1ec12bbbfebf16c41e9237b0fbc5ca81e063fac6c99887100fe7e47f43ae9b60ec2147966d962df019e3ef7375f1b9b3def34c58b49835fed28a5919e5482913131c3c9270ad6d9e3870b414cef96579475b5ef643746bab8ea11f6f775ba40c86f2266332c60cb1680123aeffed7143843d9c554524ffeda118623242b8ddb8ac82834ff148769dc83e54916516b28dff55aae2aa670ac67681e59d8237e7eadf36cb558e77cf2d14a56c6cfcc8eb4b2a5a3071e69e2e0a63f4c3e84961f2ca91ffe427927b502b144c72f43f61f9feb259ca8dac7aab04bf4b8b12a1a7ecc7d95077ac6dce1c2a292ca4a8c9702d737673aef0fc14df7057d5f7fcf7eb7b350a7774f741ae7ed8c40cd292f477ed0cafa8fba8d603e4b349c9b42712c3e5816d016eef2eebbc472c6711438ac4e1a8a314c7ffd";

var url = "https://amoproxy.onrender.com/proxy/";
let dealsArray = [];

async function loadDeals(limit = 5, page = 1) {
    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    if (page === 1) {
        dealsArray = [];
    }

    if (limit === 'all') {
        let currentPage = 1;
        let hasMoreData = true;

        while (hasMoreData) {
            try {
                const response = await axios.get(`${url}leads?limit=5&page=${currentPage}`, {
                    headers: {
                        'Authorization': 'Bearer ' + accessToken,
                        'Content-Type': 'application/json'
                    }
                });

                if (response.data._embedded && response.data._embedded.leads.length > 0) {
                    dealsArray = dealsArray.concat(response.data._embedded.leads);
                    currentPage++;
                    displayDeals(dealsArray);
                    updatePaginationButtons('');
                    await sleep(2000);
                } else {
                    hasMoreData = false;
                }
            } catch (error) {
                console.error('Ошибка при получении сделок:', error);
                hasMoreData = false;
            }
        }

    } else {
        try {
            const response = await axios.get(`${url}leads?limit=${limit}&page=${page}`, {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/json'
                }
            });

            if (response.data._embedded && response.data._embedded.leads.length > 0) {
                dealsArray = response.data._embedded.leads;
                displayDeals(dealsArray);
                updatePaginationButtons(response.data._links);
            } else {
                //console.log("Нет сделок для отображения.");
                displayDeals([]);
            }
        } catch (error) {
            console.error('Ошибка при получении сделок:', error);
        }
    }
}

function sortDeals() {
    const sortValue = document.getElementById('sortSelect').value;
    if (!sortValue) {
        //console.log("Сортировка не выбрана. Пожалуйста, выберите параметр сортировки.");
        return;
    }
    let direction = sortValue.split('-')[1];
    let field = sortValue.split('-')[0];

    let compareFunction;
    if (field === 'name') {
        compareFunction = (a, b) => {
            if (a.name < b.name) return direction === 'asc' ? -1 : 1;
            if (a.name > b.name) return direction === 'asc' ? 1 : -1;
            return 0;
        };
    } else if (field === 'price') {
        compareFunction = (a, b) => {
            return direction === 'asc' ? a.price - b.price : b.price - a.price;
        };
    }

    dealsArray.sort(compareFunction);
    displayDeals(dealsArray);
}

function updatePaginationButtons(links) {
    const paginationContainer = document.getElementById('paginationButtons');
    paginationContainer.innerHTML = '';

    Object.keys(links).forEach(key => {
        if (links[key] && key !== 'self') {
            const button = document.createElement('button');
            
            let buttonText = '';
            switch (key) {
                case 'prev':
                    buttonText = 'Предыдущая';
                    break;
                case 'next':
                    buttonText = 'Следующая';
                    break;
                case 'first':
                    buttonText = 'На первую';
                    break;
                default:
                    buttonText = key.toUpperCase();
            }
            
            button.textContent = buttonText;
            button.onclick = () => handlePaginationButtonClick(links[key].href);
            paginationContainer.appendChild(button);
        }
    });
}

function handlePaginationButtonClick(href) {
    const url = new URL(href);
    const limit = url.searchParams.get("limit");
    const page = url.searchParams.get("page");
    loadDeals(limit, page);
}

function displayDeals(deals) {
    const tableBody = document.getElementById('dealsTable').getElementsByTagName('tbody')[0];
    tableBody.innerHTML = '';

    deals.forEach(deal => {
        let row = tableBody.insertRow();
        row.insertCell(0).textContent = deal.name;
        row.insertCell(1).textContent = deal.price;
        row.insertCell(2).textContent = new Date(deal.created_at * 1000).toLocaleString();
        row.insertCell(3).textContent = new Date(deal.updated_at * 1000).toLocaleString();
        row.insertCell(4).textContent = deal.responsible_user_id;
    });
}

loadDeals();
